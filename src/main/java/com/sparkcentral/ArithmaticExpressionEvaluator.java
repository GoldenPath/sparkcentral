/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sparkcentral;

import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author hemant
 */
public class ArithmaticExpressionEvaluator {

    
    static String expression = "2+3+4";
    
    public static void main(String[] args) {

        //Create stacks for operators and values
        Stack<Character> operations = new Stack<Character>();
        Stack<Double> values = new Stack<Double>();
        //Create temporary stacks for operators and operands
        Stack<Character> operationsTemp = new Stack<Character>();
        Stack<Double> valuesTemp = new Stack<Double>();

        
        Set<Character> operationSet = Operation.getOperatorSet();

        //Store values and operators in respective stacks
        String temp = "";
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            if (operationSet.contains(ch)) {
                temp = temp + ch;
            } else {
                values.push(Double.parseDouble(temp));
                operations.push(ch);
                temp = "";
            }
        }
        values.push(Double.parseDouble(temp));

        //Evaluation of expression
        for (Operation operation:Operation.values()) {

            while (!operations.isEmpty()) {
                char optr = operations.pop();
                double v1 = values.pop();
                double v2 = values.pop();
                if (optr == operation.getOperator()) {
                    //if operator matches evaluate and store in temporary stack
                        valuesTemp.push(operation.getEvaluator().evaluate(v2,v1));
                        break;
                } else {
                    valuesTemp.push(v1);
                    values.push(v2);
                    operationsTemp.push(optr);
                }
            }
            //Push back all elements from temporary stacks to main stacks
            while (!valuesTemp.isEmpty()) {
                values.push(valuesTemp.pop());
            }
            while (!operationsTemp.isEmpty()) {
                operations.push(operationsTemp.pop());
            }
        }
        System.out.println("Result = " + values.pop());
    }

}
