/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sparkcentral.evaluator;

/**
 *
 * @author hemant
 */
public class AdditionEvaluator implements Evaluator {

    @Override
    public Double evaluate(Double input1, Double input2) throws ArithmeticException {
        return input1+input2;
    }
    
}
