/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sparkcentral;

import com.sparkcentral.evaluator.AdditionEvaluator;
import com.sparkcentral.evaluator.DivisionEvaluator;
import com.sparkcentral.evaluator.Evaluator;
import com.sparkcentral.evaluator.MultiplicationEvaluator;
import com.sparkcentral.evaluator.SubstractionEvaluator;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author hemant
 */
public enum Operation {
    
    // Sequence of the enum is the precedence;
    DIVISION('/', new DivisionEvaluator()),
    MULTIPLICATION('*', new MultiplicationEvaluator()),
    ADDITION('+',new AdditionEvaluator()),
    SUBTRACTION('-', new SubstractionEvaluator());
    
    
    
    public char operator;
    public Evaluator evaluator; 

    private Operation(char operand, Evaluator evaluator) {
        this.operator = operand;
        this.evaluator = evaluator;
    }
    
    
    public char getOperator() {
        return operator;
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }
    
    
    //Can be cached
    public static Set<Character> getOperatorSet(){
        
        Set operatorSet = new HashSet();
        for(Operation operation:values()){
            operatorSet.add(operation);
        }
        return operatorSet;
    }
    

}
